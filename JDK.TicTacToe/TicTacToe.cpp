#include <iostream>

#include "TicTacToe.h"

using namespace std;

TicTacToe::TicTacToe()
{
	for (int i = 0; i < 9; i++)
	{
		m_board[i] = ' ';
	}

	m_numTurns = 0;
	m_playerTurn = 'O';
	m_winner = '\0';
}

void TicTacToe::DisplayBoard() const
{
	for (int i = 0; i < 5; i++)
	{
		if (i % 2 == 0)
		{
			const int offset = i + ((i + i) % 3);
			cout << m_board[offset] << '|' << m_board[offset + 1] << '|' << m_board[offset + 2] << endl;
		}
		else
		{
			cout << "-----" << endl;
		}
	}
}

bool TicTacToe::IsOver()
{	
	for (int i = 0; i < 8; i++) {
		char firstToken = m_board[m_winCombos[i][0]];
		bool win = true;

		if (firstToken == ' ') continue; // Ignore any combos that start with an empty space

		for (int n = 1; n < 3; n++) {
			if (firstToken != m_board[m_winCombos[i][n]]) {
				win = false;
				break;
			}
		}

		if (win)
		{
			// Hacky swap to work with Move() which swaps tokens every move
			if (m_playerTurn == 'O')
			{
				m_winner = 'X';
			}
			else {
				m_winner = 'O';
			}
			return true;
		}
	}

	if (m_numTurns == 9)
	{
		return true;
	}

	return false;
}

char* TicTacToe::GetPlayerTurn()
{
	return &m_playerTurn;
}

bool TicTacToe::IsValidMove(const int position) const
{
	return m_board[position - 1] == ' ';
}

void TicTacToe::Move(const int position)
{
	char* currPlayer = GetPlayerTurn();
	m_board[position - 1] = *currPlayer;

	m_numTurns++;

	// Swap the player
	if (*currPlayer == 'O') {
		*currPlayer = 'X';
	}
	else {
		*currPlayer = 'O';
	}
}

void TicTacToe::DisplayResult() const
{
	if (m_winner == '\0')
	{
		cout << "It was a tie." << endl;
	}
	else
	{
		cout << m_winner << " wins!" << endl;
	}
}
