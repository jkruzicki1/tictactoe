#pragma once
class TicTacToe
{
public:
	TicTacToe();

	void DisplayBoard() const;
	bool IsOver();
	char* GetPlayerTurn();
	bool IsValidMove(const int position) const;
	void Move(const int position);
	void DisplayResult() const;
private:
	char m_board[9];
	int m_numTurns;
	char m_playerTurn;
	char m_winner;

	const int m_winCombos[8][3] = {
		{0, 1, 2},
		{3, 4, 5},
		{6, 7, 8},
		{0, 3, 6},
		{1, 4, 7},
		{2, 5, 8},
		{0, 4, 8},
		{2, 4, 6}
	};
};
